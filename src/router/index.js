import Vue from 'vue'
import Router from 'vue-router'
import IndexPage from '../components/index-page/index-page'
import MainPage from '../components/main-page/main-page'
import HeaderPage from '../components/header-page/header-page'
import FooterPage from '../components/footer-page/footer-page'
import TimeLimited from '../components/main-page/components/time-limited/time-limited'
import FreeLesson from '../components/main-page/components/free-lesson/free-lesson'
import ActualCombat from '../components/main-page/components/actual-combat/actual-combat'
import DiscussionArea from '../components/main-page/components/discussion-area/discussion-area'
import DiscussionAreaPage from '../components/main-page/pages/discussion-area-page/discussion-area-page'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index-page',
      component: IndexPage
    },
    // 主页
    {
      path: '/main-page',
      name: 'main-page',
      component: MainPage
    },
    {
      path: '/header-page',
      name: 'header-page',
      component: HeaderPage
    },
    {
      path: '/footer-page',
      name: 'footer-page',
      component: FooterPage
    },
    {
      path: '/time-limited',
      name: 'time-limited',
      component: TimeLimited
    },
    {
      path: '/free-lesson',
      name: 'free-lesson',
      component: FreeLesson
    },
    {
      path: '/actual-combat',
      name: 'actual-combat',
      component: ActualCombat
    },
    {
      path: '/discussion-area',
      name: 'discussion-area',
      component: DiscussionArea
    },
    {
      path: '/main-page/discussion-area-page',
      name: 'discussion-area-page',
      component: DiscussionAreaPage
    }
  ]
})
