import http from './http'

export default {
  login (params) {
    return http.get('/login', params)
  }
}
